## 介绍

该项目基于Java开发。 

* 后端采用Spring Boot、Spring Data Jpa、Sa Token、 Redis、Mysql。
* 前端采用Vue、Quasar。


## 在线体验

##### 管理后台：http://114.132.76.154:8081/page/login
账号：admin，密码：123 


## 演示截图
<table>
    <tr>
        <td><img src="https://gitee.com/muyu8848/muyu-fast/raw/master/doc/img/1.jpg"/></td>
        <td><img src="https://gitee.com/muyu8848/muyu-fast/raw/master/doc/img/2.jpg"/></td>
	<td><img src="https://gitee.com/muyu8848/muyu-fast/raw/master/doc/img/3.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/muyu8848/muyu-fast/raw/master/doc/img/4.jpg"/></td>
        <td><img src="https://gitee.com/muyu8848/muyu-fast/raw/master/doc/img/5.jpg"/></td>
	<td><img src="https://gitee.com/muyu8848/muyu-fast/raw/master/doc/img/6.jpg"/></td>
    </tr>
</table>


