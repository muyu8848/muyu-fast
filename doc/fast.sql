/*
 Navicat Premium Data Transfer

 Source Server         : local_mysql
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : fast

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 14/11/2022 12:52:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account_role
-- ----------------------------
DROP TABLE IF EXISTS `account_role`;
CREATE TABLE `account_role`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `account_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

-- ----------------------------
-- Table structure for background_account
-- ----------------------------
DROP TABLE IF EXISTS `background_account`;
CREATE TABLE `background_account`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `deleted_flag` bit(1) DEFAULT NULL,
  `deleted_time` datetime(0) DEFAULT NULL,
  `google_auth_bind_time` datetime(0) DEFAULT NULL,
  `google_secret_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `lately_login_time` datetime(0) DEFAULT NULL,
  `login_pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `registered_time` datetime(0) DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `super_admin_flag` bit(1) DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

-- ----------------------------
-- Records of background_account
-- ----------------------------
BEGIN;
INSERT INTO `background_account` VALUES ('1', b'0', NULL, NULL, NULL, '2022-11-14 12:10:44', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '2022-02-16 17:44:03', '1', b'1', 'admin', 185);
COMMIT;

-- ----------------------------
-- Table structure for dict_item
-- ----------------------------
DROP TABLE IF EXISTS `dict_item`;
CREATE TABLE `dict_item`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dict_item_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `dict_item_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `dict_type_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `order_no` double DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

-- ----------------------------
-- Records of dict_item
-- ----------------------------
BEGIN;
INSERT INTO `dict_item` VALUES ('1496402213579259904', '1', '成功', '1496402134919282688', 1, 0), ('1496402213583454208', '0', '失败', '1496402134919282688', 2, 0), ('1503357286762938368', '1', '启用', '1499723556546674688', 1, 0), ('1503357286767132672', '0', '禁用', '1499723556546674688', 2, 0), ('1505122214314246144', '1', '未发送', '1505122154398613504', 1, 0), ('1505122214314246145', '2', '发送成功', '1505122154398613504', 2, 0), ('1505122214318440448', '3', '发送失败', '1505122154398613504', 3, 0), ('1591632543751340032', 'login', '验证码_登录', '1505114508769624064', 1, 0), ('1591632582305382400', 'admin', '管理后台', '1501914563405152256', 1, 0);
COMMIT;

-- ----------------------------
-- Table structure for dict_type
-- ----------------------------
DROP TABLE IF EXISTS `dict_type`;
CREATE TABLE `dict_type`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dict_type_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `dict_type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `last_modify_time` datetime(0) DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

-- ----------------------------
-- Records of dict_type
-- ----------------------------
BEGIN;
INSERT INTO `dict_type` VALUES ('1496402134919282688', 'loginState', '登录状态', '2022-02-23 16:31:04', '', 1), ('1499723556546674688', 'functionState', '功能状态', '2022-03-14 21:08:03', '', 4), ('1501914563405152256', 'subSystem', '子系统', '2022-11-13 11:22:33', '', 5), ('1505114508769624064', 'smsType', '短信类型', '2022-11-13 11:22:24', '', 3), ('1505122154398613504', 'smsSendState', '短信发送状态', '2022-03-19 18:01:14', '', 1);
COMMIT;

-- ----------------------------
-- Table structure for login_log
-- ----------------------------
DROP TABLE IF EXISTS `login_log`;
CREATE TABLE `login_log`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `browser` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ip_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `login_time` datetime(0) DEFAULT NULL,
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `os` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `sub_system` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `deleted_flag` bit(1) DEFAULT NULL,
  `deleted_time` datetime(0) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `order_no` double DEFAULT NULL,
  `parent_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

-- ----------------------------
-- Records of menu
-- ----------------------------
BEGIN;
INSERT INTO `menu` VALUES ('1240880886262530048', b'0', NULL, '系统日志数据', 11, '', 'menu_1', ''), ('1240881072485433344', b'0', NULL, '登录日志', 2, '1240880886262530048', 'menu_2', '/page/login-log'), ('1240881126407405568', b'0', NULL, '操作日志', 2, '1240880886262530048', 'menu_2', '/page/oper-log'), ('1240881494369501184', b'0', NULL, '后台管理', 13, '', 'menu_1', ''), ('1240881668613472256', b'0', NULL, '后台菜单', 3, '1240881494369501184', 'menu_2', '/page/menu-manage'), ('1241121324122767360', b'0', NULL, '后台角色', 2, '1240881494369501184', 'menu_2', '/page/role-manage'), ('1241121744119398400', b'0', NULL, '后台账号', 1, '1240881494369501184', 'menu_2', '/page/background-account'), ('1431995012924571648', b'0', NULL, '超级管理员', 0.5, '1240881494369501184', 'menu_2', '/page/super-admin'), ('1505120569647955968', b'0', NULL, '短信发送情况', 1.3, '1240880886262530048', 'menu_2', '/page/sms-send-record'), ('1506199470319075328', b'0', NULL, '系统设置', 12, '', 'menu_1', ''), ('1577880284160851968', b'0', NULL, 'IP黑名单', 2.2, '1506199470319075328', 'menu_2', '/page/ip-black-list'), ('1581930933164965888', b'0', NULL, '数据字典', 2.2, '1506199470319075328', 'menu_2', '/page/dict-manage'), ('1592007221284175872', b'0', NULL, '在线账号', 1, '', 'menu_1', '/page/online-account');
COMMIT;

-- ----------------------------
-- Table structure for oper_log
-- ----------------------------
DROP TABLE IF EXISTS `oper_log`;
CREATE TABLE `oper_log`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ip_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `module` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `oper_account_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `oper_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `oper_time` datetime(0) DEFAULT NULL,
  `operate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `request_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `request_param` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `request_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `sub_system` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `create_time` datetime(0) DEFAULT NULL,
  `deleted_flag` bit(1) DEFAULT NULL,
  `deleted_time` datetime(0) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `menu_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

-- ----------------------------
-- Table structure for sms_send_record
-- ----------------------------
DROP TABLE IF EXISTS `sms_send_record`;
CREATE TABLE `sms_send_record`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `create_time` datetime(0) DEFAULT NULL,
  `error_msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `send_time` datetime(0) DEFAULT NULL,
  `sms_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `verification_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

SET FOREIGN_KEY_CHECKS = 1;
