package com.muyu.fast;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.muyu.fast.constants.Constant;
import com.muyu.fast.sms.service.SmsService;
import com.zengtengpeng.annotation.MQListener;

@Component
public class RedisMqListener {

	@Autowired
	private SmsService smsService;

	@MQListener(name = Constant.发送短信)
	public void sendSms(String topicName, String value) {
		smsService.sendSms(value);
	}

}
