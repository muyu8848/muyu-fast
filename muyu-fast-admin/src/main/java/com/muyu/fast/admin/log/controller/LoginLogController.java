package com.muyu.fast.admin.log.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.muyu.fast.common.vo.PageResult;
import com.muyu.fast.common.vo.Result;
import com.muyu.fast.log.param.LoginLogQueryCondParam;
import com.muyu.fast.log.service.LoginLogService;
import com.muyu.fast.log.vo.LoginLogVO;

@Controller
@RequestMapping("/loginLog")
public class LoginLogController {

	@Autowired
	private LoginLogService loginLogService;

	@GetMapping("/findLoginLogByPage")
	@ResponseBody
	public Result<PageResult<LoginLogVO>> findLoginLogByPage(LoginLogQueryCondParam param) {
		return Result.success(loginLogService.findLoginLogByPage(param));
	}

}
