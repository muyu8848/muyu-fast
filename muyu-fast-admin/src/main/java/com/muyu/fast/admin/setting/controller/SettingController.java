package com.muyu.fast.admin.setting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.muyu.fast.common.vo.Result;
import com.muyu.fast.setting.service.SettingService;

@Controller
@RequestMapping("/setting")
public class SettingController {

	@Autowired
	private SettingService service;

	@PostMapping("/refreshCache")
	@ResponseBody
	public Result<String> refreshCache() {
		service.refreshCache();
		return Result.success();
	}

}
