package com.muyu.fast.admin.sms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.muyu.fast.common.vo.PageResult;
import com.muyu.fast.common.vo.Result;
import com.muyu.fast.sms.param.SmsSendRecordQueryCondParam;
import com.muyu.fast.sms.service.SmsService;
import com.muyu.fast.sms.vo.SmsSendRecordVO;

@RestController
@RequestMapping("/sms")
public class SmsController {

	@Autowired
	private SmsService smsService;

	@GetMapping("/findSendRecordByPage")
	public Result<PageResult<SmsSendRecordVO>> findSendRecordByPage(SmsSendRecordQueryCondParam param) {
		return Result.success(smsService.findSendRecordByPage(param));
	}

}
