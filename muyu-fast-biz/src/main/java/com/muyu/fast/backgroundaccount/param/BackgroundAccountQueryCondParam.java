package com.muyu.fast.backgroundaccount.param;

import com.muyu.fast.common.param.PageParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class BackgroundAccountQueryCondParam extends PageParam {
	
	private String userName;

}
