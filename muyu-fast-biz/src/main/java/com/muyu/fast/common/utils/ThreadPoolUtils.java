package com.muyu.fast.common.utils;

import java.util.concurrent.ScheduledThreadPoolExecutor;

public class ThreadPoolUtils {

	private static ScheduledThreadPoolExecutor sendSmsPool = new ScheduledThreadPoolExecutor(4);

	public static ScheduledThreadPoolExecutor getSendSmsPool() {
		return sendSmsPool;
	}

}
