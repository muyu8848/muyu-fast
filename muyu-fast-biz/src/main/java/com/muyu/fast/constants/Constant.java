package com.muyu.fast.constants;

public class Constant {
	
	public static final String IP黑名单 = "ipBlackList";

	public static final String 发送短信 = "sendSms";

	public static final String 短信发送状态_未发送 = "1";

	public static final String 短信发送状态_发送成功 = "2";

	public static final String 短信发送状态_发送失败 = "3";

	public static final String 限制 = "limit";

	public static final String 短信类型_验证码_登录 = "login";

	public static final String 菜单类型_一级菜单 = "menu_1";

	public static final String 菜单类型_二级菜单 = "menu_2";

	public static final String 子系统_后台管理 = "admin";

	public static final String 登录提示_登录成功 = "登录成功";

	public static final String 登录状态_成功 = "1";

	public static final String 登录状态_失败 = "0";

	public static final String 功能状态_启用 = "1";

	public static final String 功能状态_禁用 = "0";

}
