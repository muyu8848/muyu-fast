package com.muyu.fast.dictconfig.param;

import com.muyu.fast.common.param.PageParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class DictTypeQueryCondParam extends PageParam {
	
	private String dictTypeCode;

	private String dictTypeName;

}
